import { FETCH_PRODUCTS,
         GET_PRODUCT_DETAILS,
         ADD_ITEM_TO_CART,
         REMOVE_ITEM_FROM_CART,
         UPDATE_ITEM_QUANTITY,
         SET_PRODUCT_TAG_FILTER, 
         CHECKOUT,
         RESET_CART} from '../actionTypes'

export const fetchProducts = () => {
  return ({ type: FETCH_PRODUCTS })
}

export const getProductDetails = (id) => {
  return ({ type: GET_PRODUCT_DETAILS, id })
}

export const addItemToCart = (id, quantity = 1) => {
  return ({ type: ADD_ITEM_TO_CART, id, quantity })
}

export const updateItemQuantity = (id, quantity) => {
  if (quantity === '') quantity = 0
  return ({ type: UPDATE_ITEM_QUANTITY, id, quantity })
}

export const removeItemFromCart = (id) => {
  return ({ type: REMOVE_ITEM_FROM_CART, id })
}

export const filterProducts = (tag) => {
  return ({ type: SET_PRODUCT_TAG_FILTER, tag})
}

export const checkout = (items) => {
  items = items.map(item => {
    return {
      id: item.id,
      quantity: item.quantity
    }
  })
  return ({ type: CHECKOUT, items})
}

export const resetCart = () => {
  return ({ type: RESET_CART })
}
