import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import PropTypes from 'prop-types'

class Product extends Component {

  constructor(props) {
    super(props)
    this.handleAddItem = this.handleAddItem.bind(this)
  }

  handleAddItem(id) {
    let quantity = document.getElementById(`${id}_quantity`).value
    this.props.addItemToCart(id, quantity)
  }

  render() {
    const {product} = this.props
    return (
      <Card>
        <CardContent>
        <img className='thumbnail' src={product.thumbnail} />
        <div className='product-details'>
          <NavLink className='title' to={`/detail/${product.id}`}>{product.title}</NavLink>
          <div className={`price ${product.inStock? '': 'out-of-stock'}`}>${product.price}</div>
          <div className='purchase-options'>
            <input disabled={!product.inStock}  size='3' id={`${product.id}_quantity`} className='quantity' type='text' defaultValue='1' />
            <Button  variant='outlined'  disabled={!product.inStock} onClick={()=> this.handleAddItem(product.id)}>Add To Cart</Button>
          </div>
        </div>
        </CardContent>
      </Card>
    )
  }
}

Product.propTypes = {
  addItemToCart: PropTypes.func.isRequired
}

export default Product
