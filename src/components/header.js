import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

class Header extends Component {

  render() {
    const {cartTotalCount} = this.props

    return (
      <header className='header'>
        <h1 className='logo'><NavLink to={`/`}>Cool Stuff</NavLink></h1>
        <NavLink className='cart' to={`/cart`}><div >Cart:{cartTotalCount}</div></NavLink>
      </header> 
    )
  }
}

Header.propTypes = {
  cartTotalCount: PropTypes.number.isRequired
}

export default Header