import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import Button from '@material-ui/core/Button'

import LineItem from './cartLineItem'
import Total from './totals'
import { getProduct } from '../reducers/productReducer'

class Cart extends Component {

  componentWillUnmount() {
    if (this.props.checkoutConfirmation) {
      this.props.resetCart()
    }
  }

  handleCheckout() {
    this.props.checkout(this.props.items)
  }

  renderEmptyCart() {
    return (
      <div>
        You have no items in your cart. Get some <NavLink to={`/`}>Cool Stuff</NavLink>
      </div>
    )
  }

  renderCart() {
    return (
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Qty.</th>
            <th>Price</th>
            <th>Total</th>
            <th>{/*empty*/}</th>
          </tr>
        </thead>
        <tbody>
          {this.props.items.map( item => {
            return(
              <LineItem key={item.id} item={item} {...this.props}  />
            )
          })}
        </tbody>
      </table>
    )
  }

  renderSubTotal() {
    return <Total {...this.props} />
  }

  renderCheckout() {
    return <Button disabled={this.props.loading}  variant='outlined' color='primary' onClick={() => this.handleCheckout()}>Checkout</Button>
  }

  renderConfirmation() {
    return <h2>{this.props.checkoutConfirmation}</h2>
  }
  
  renderErrorMessage() {
    return <div className='error'>{this.props.error}</div>
  }

  renderLoading() {
    return <div className='processing-checkout'>Processing...</div>
  }


  render() {
    const {items, checkoutConfirmation, error, loading} = this.props
    if(!checkoutConfirmation) {
      return(
        <div className='cart-container'>
          {!items.length && this.renderEmptyCart()}
          {!!items.length && this.renderCart()}
          {!!items.length && this.renderSubTotal()}
          {!!items.length && this.renderCheckout()}
          {loading && this.renderLoading()}
          {error && this.renderErrorMessage()}
        </div>
      )
    } else {
      return(
        <div className='cart-container'>
          {this.renderConfirmation()}
        </div>
      )
    }

  }
}

// todo custom prop type for object
Cart.propTypes = {
  items: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  checkoutConfirmation: PropTypes.string.isRequired,
  handleUpdate: PropTypes.func.isRequired,
  handleRemove: PropTypes.func.isRequired,
  checkout: PropTypes.func.isRequired,
  resetCart: PropTypes.func.isRequired
}

export default Cart