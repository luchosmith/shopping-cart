import React, { Component } from 'react'

export default ({items}) => {

  const tax = 8.875
  const shipping = 9.99

  function getTaxAmount(amount) {
    return (amount * (tax/100))
  }

  let total = items.reduce((total, item) => {
    total += item.product.price * item.quantity
    return total
  },0)

  if(!items.length) {
    return null
  }

  return(
    <div className='totals'>
      <div>subotal: {total.toFixed(2)}</div>
      <div>tax: {getTaxAmount(total).toFixed(2)}</div>
      <div>shipping: {(shipping).toFixed(2)}</div>
      <div><strong>grand total: {`$${(total + getTaxAmount(total) + shipping).toFixed(2) }`}</strong></div>
    </div>
  )
}