import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import Chip from '@material-ui/core/Chip'
import Reviews from './reviews'

class ProductDetail extends Component {

  componentWillMount() {
    if(!this.props.productDetail.id || this.props.productDetail.id !== this.props.match.params.id ) {
      this.props.getProductDetails(this.props.match.params.id)
    }
  }

  handleAddItem(id) {
    let quantity = document.getElementById(`${id}_quantity`).value
    this.props.addItemToCart(id, quantity)
  }

  handleTagClick(tag) {
    this.props.filterProducts(tag)
    this.props.history.push('/')
  }

  renderProductDetails() {
    const product = this.props.productDetail
    return (
      <div>
        <h2>{product.title}</h2>
        {product.images.map((image, index) => {
          return (
            <img key={`image-${index}`} src={image} />
          )
        })}
        <div className='product-description'>
          <p>{product.description}</p>
          {product.tags.map((tag, index) =>{
            return (
              <Chip key={`tag-${index}`} label={tag} className='tag' onClick={()=> this.handleTagClick(tag)} />
            )
          })}
          <Reviews reviews={product.reviews} />
          <div className='purchas-options'>
            <span>${product.price}</span>
            <span>{product.inStock ? 'In Stock':'Out of stock'}</span>
            <input size='3' disabled={!product.inStock} id={`${product.id}_quantity`} className='quantity' type='text' defaultValue='1' />
            <Button variant='outlined'  disabled={!product.inStock} onClick={()=> this.handleAddItem(product.id)}>Add To Cart</Button>
          </div>
        </div>
      </div>
    )
  }

  renderNoDetails() {
    return (
      <div>
        Product not found
      </div>
    )
  }

  render() {
    const {productDetail} = this.props
    return(
      <div className='product-detail-container'>
      <NavLink to={`/`}><Button variant='outlined' >Back</Button></NavLink>
      {productDetail && productDetail.id && this.renderProductDetails()}
      {!productDetail.id && this.renderNoDetails()}
      </div>
    )
  }
}

ProductDetail.propTypes = {
  productDetail: PropTypes.object.isRequired,
  addItemToCart: PropTypes.func.isRequired,
  filterProducts: PropTypes.func.isRequired
}

export default ProductDetail