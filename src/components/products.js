import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Product from './product'
import Chip from '@material-ui/core/Chip'

// TODO: error state, clickCallback
class Products extends Component {

  componentWillMount() {
    if(!this.props.products.length) {
      this.props.fetchProducts()
    }
  }

  renderFilter() {
    return(
      <div className='product-filter'>
        Filter: <Chip onDelete={() => this.props.filterProducts('')} label={this.props.tagFilter} />
      </div>
    )
  }

  renderError(error) {
    return(<div>{`Error: ${error}`}</div>)
  }

  renderLoading() {
    return(<div>Loading...</div>)
  }

  renderProducts(products) {
    return (
      <div className='product-container'>
        {this.props.tagFilter && this.renderFilter()}
        <div className='product-grid'>
            {products.map(product => {
              return (
                <div key={product.id} className='product'> 
                      <Product product={product} addItemToCart={this.props.addItemToCart} filterProducts={this.props.filterProducts} />
                </div>
              )
            })}  
        </div>
      </div>
    )
  }

  render() {
    const {products, loading, error} = this.props

    return (
      <div>
        { error && this.renderError(error) }
        { loading && this.renderLoading() }
        { !!products.length && this.renderProducts(products) }
      </div> 
    )
  }
}

Products.propTypes = {
  products: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  tagFilter: PropTypes.string,
  fetchProducts: PropTypes.func.isRequired,
  addItemToCart: PropTypes.func.isRequired,
  filterProducts: PropTypes.func.isRequired
}

export default Products