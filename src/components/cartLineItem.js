import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'

class LineItem extends Component {

  constructor(props) {
    super(props)
    this.update = this.update.bind(this)
    this.remove = this.remove.bind(this)
  }

  update(e) {
    let value = e.target.value || 0
    this.props.handleUpdate(this.props.item.id, value)
  }

  remove() {
    this.props.handleRemove(this.props.item.id)
  }
  
  render() {
    const {item} = this.props
    return (
      <tr className='cart-line-item'>
        <td className='title'>{item.product.title}</td>
        <td className='quantity'>
          <input className='quantity' type='text' size='3' value={item.quantity} onChange={this.update} />
        </td>
        <td className='price'>{item.product.price}</td>
        <td className='total'>{(item.product.price * item.quantity).toFixed(2)}</td>
        <td className='remove'><Button variant='outlined' onClick={this.remove}>Remove</Button></td>
      </tr>
    )
  }

}

LineItem.propTypes = {
  item: PropTypes.object.isRequired,
  handleUpdate: PropTypes.func.isRequired,
  handleRemove: PropTypes.func.isRequired
}

export default LineItem

