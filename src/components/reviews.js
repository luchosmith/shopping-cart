import React, { Component } from 'react'

export default ({reviews}) => {

  const maxRating = 5

  if(!reviews.length) {
    return null
  }

  function averageReviews(reviews) {
    let total = reviews.reduce((total, review) => total + review.rating , 0)
    let average = total / reviews.length

    if (average % 1 !== 0) {
      return average.toFixed(1)
    }
    return average
  }

  return(
    <div className='average-reviews'>
      Reviews: {`${averageReviews(reviews)} out of ${maxRating} (${reviews.length})` }
    </div>
  )
}