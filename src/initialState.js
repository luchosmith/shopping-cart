export default {
  product: {
    products: [],
    tagFilter: '',
    error: '',
    loading: false,
    productDetail: {}
  },
  cart: {
    items: [],
    loading: false,
    error: '',
    checkoutConfirmation: ''
  }
  
}