import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

import configureStore from './store'
import rootSaga from './sagas'
import mainReducer from './reducers'
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom'

import Products from './containers/productContainer'
import ProductDetail from './containers/productDetailContainer'
import Header from './containers/headerContainer'
import Cart from './containers/cartContainer'

const store = configureStore()

class Root extends Component {

    constructor() {
        super()
        this.store = store
        this.state = this.store.getState()
    }


    render() {
        return (
            <Provider store={this.store}>
                <Router>
                    <MuiThemeProvider theme={createMuiTheme()}>
                        <Header />
                        <div className='main-content'>
                            <Route exact path='/' component={Products} />
                            <Route path='/detail/:id' component={ProductDetail} />
                            <Route path='/cart' component={Cart} /> 
                        </div>
                    </MuiThemeProvider>
                </Router>
            </Provider>
        )
    }
}

ReactDOM.render((<Root />), document.getElementById('react-root'))
