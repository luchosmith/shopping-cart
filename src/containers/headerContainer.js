import { connect } from 'react-redux'
import Header from '../components/header'
import { getCartTotalCount } from '../reducers/cartReducer'

const mapStateToProps = state => {
  return {
    cartTotalCount: getCartTotalCount(state.cart)
  }
}

export default connect(
  mapStateToProps
)(Header)