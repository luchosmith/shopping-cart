import { connect } from 'react-redux'
import Cart from '../components/cart.js'
import { updateItemQuantity, removeItemFromCart, checkout, resetCart } from '../actions/'
import { getCartItems } from '../reducers/cartReducer'
import { getProduct } from '../reducers/productReducer'

const mapStateToProps = state => {
  let items = getCartItems(state.cart).map(item => {
    return {
      ...item,
      product: getProduct(item.id, state.product)
    }
  })
  return {
    items: items,
    loading: state.cart.loading,
    error: state.cart.error,
    checkoutConfirmation: state.cart.checkoutConfirmation
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleUpdate: (id, quantity) => {
      dispatch(updateItemQuantity(id, quantity))
    },
    handleRemove: (id) => {
      dispatch(removeItemFromCart(id))
    },
    checkout: (items) => {
      dispatch(checkout(items))
    },
    resetCart: () => {
      dispatch(resetCart())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)