import { connect } from 'react-redux'
import Products from '../components/products'
import { fetchProducts, addItemToCart, filterProducts }from '../actions'
import { getProducts } from '../reducers/productReducer'

///TODO: selectors
const mapStateToProps = state => {
  return {
    products: getProducts(state.product),
    tagFilter: state.product.tagFilter,
    loading: state.product.loading,
    error: state.product.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchProducts: () => {
      dispatch(fetchProducts())
    },
    addItemToCart: (id, quantity) => {
      dispatch(addItemToCart(id, quantity))
    },
    filterProducts: (tag) => {
      dispatch(filterProducts(tag))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products)