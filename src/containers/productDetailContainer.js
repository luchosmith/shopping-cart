import { connect } from 'react-redux'
import ProductDetail from '../components/productDetail'
import { getProductDetails, addItemToCart, filterProducts }from '../actions'

///TODO: selectors
const mapStateToProps = state => {
  return {
    productDetail: state.product.productDetail
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getProductDetails: (id) => {
      dispatch(getProductDetails(id))
    },
    addItemToCart: (id, quantity) => {
      dispatch(addItemToCart(id, quantity))
    },
    filterProducts: (tag) => {
      dispatch(filterProducts(tag))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetail)