
const base_url = 'http://localhost:3001'

export const getItems = () => {
  return fetch(`${base_url}/get-items`,  {
    credentials: 'same-origin'
  })
  .then(response => response.json())
  .then(data => {
    return {
      products: data
    }
  })
  .catch(error => {error:error})
}

export const checkout = (items) => {
  return fetch(`${base_url}/checkout`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    body: JSON.stringify(items),
    credentials: 'same-origin'
  })
  .then(response => response.json())
  .then(data => {
    return data
  })
  .catch(error => {error:error})
}
