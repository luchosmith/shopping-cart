import { all, call, fork } from 'redux-saga/effects'
import productsSaga from './productsSaga'
import checkoutSaga from './checkoutSaga'

export default function* rootSaga() {
  yield all([
    fork(productsSaga),
    fork(checkoutSaga)
  ])
}