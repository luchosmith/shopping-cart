import { put, call, take, takeEvery } from 'redux-saga/effects'
import { delay } from 'redux-saga/'

import { FETCH_PRODUCTS,
         FETCH_PRODUCTS_LOADING,
         FETCH_PRODUCTS_ERROR,
         FETCH_PRODUCTS_SUCCESS,
         CHECKOUT,
         CHECKOUT_LOADING,
         CHECKOUT_ERROR,
         CHECKOUT_SUCCESS } from '../actionTypes'

import { checkout } from '../lib/mock-api'
import { getCartItems } from '../reducers/cartReducer'

function* checkoutSaga({items}) {
  yield put({ type: CHECKOUT_LOADING })

  try {
    yield delay(1000)
    let payload = {items: items}
    const {status, message} = yield call(checkout, payload)
    if(status === 'success') {
      yield put({ type: CHECKOUT_SUCCESS, message})
    }
    else throw ('Error processing checkout.')
  }

  catch(error) {
    error = error.message || 'Error processing checkout.'
    yield put({ type: CHECKOUT_ERROR, error})
  }

}

export default function* saga() {
  yield takeEvery(CHECKOUT, checkoutSaga)
}