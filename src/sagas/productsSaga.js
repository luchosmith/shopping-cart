import { put, call, take, takeEvery } from 'redux-saga/effects'
import { delay } from 'redux-saga/'
import { FETCH_PRODUCTS,
         FETCH_PRODUCTS_LOADING,
         FETCH_PRODUCTS_ERROR,
         FETCH_PRODUCTS_SUCCESS } from '../actionTypes'
import { getItems } from '../lib/mock-api'

function* fetchProducts() {

  yield put({ type: FETCH_PRODUCTS_LOADING })

  try {
    yield delay(1000)
    const payload = yield call(getItems)
    yield put({ type: FETCH_PRODUCTS_SUCCESS, ...payload })
  }

  catch(error) {
    yield put({ type: FETCH_PRODUCTS_ERROR, error })
  }

}

export default function* saga() {
  yield takeEvery(FETCH_PRODUCTS, fetchProducts)
}