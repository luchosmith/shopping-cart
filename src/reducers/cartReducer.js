import initialState from '../initialState'
import { ADD_ITEM_TO_CART,
         UPDATE_ITEM_QUANTITY,
         REMOVE_ITEM_FROM_CART,
         CHECKOUT_LOADING,
         CHECKOUT_ERROR,
         CHECKOUT_SUCCESS,
         RESET_CART } from '../actionTypes'

export default (state = initialState.cart, action) => {

  switch(action.type) {

    case ADD_ITEM_TO_CART: {

      let items = state.items.slice()
      let itemIndex = items.findIndex( item => item.id === action.id )

      let quantity = parseInt(action.quantity)
      if ( isNaN(quantity) ) quantity = 1

      if( itemIndex > -1 ) {
        items[itemIndex].quantity += quantity
      } else {
        items.push({
          id: action.id,
          quantity
        })
      }
      return {
        ...state,
        items
      }
    }

    case UPDATE_ITEM_QUANTITY: {
      let items = state.items.slice()
      let itemIndex = items.findIndex( item => item.id === action.id )
      if( itemIndex > -1 ) {
        items[itemIndex].quantity = parseInt(action.quantity) 
      }
      return {
        ...state,
        items
      }
    }

    case REMOVE_ITEM_FROM_CART: {
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.id)
      }
    }

    case CHECKOUT_LOADING: {
      return {
        ...state,
        loading: true
      }
    }

    case CHECKOUT_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error
      }
    }

    case CHECKOUT_SUCCESS: {
      return  {
        ...state,
        loading: false,
        error: '',
        items: [],
        checkoutConfirmation: action.message 
      }
    }

    case RESET_CART: {
      return {
        ...state,
        loading: false,
        error: '',
        items: [],
        checkoutConfirmation: ''
      }
    }

    default:
      return {...state}
  }
}

/***** SELECTORS *****/
export function getCartItems(state) {
  return state.items
}

export function getCartTotalCount(state) {
  return state.items.reduce((total, item) => {
    return total += item.quantity
  },0)
}


