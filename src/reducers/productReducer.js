import initialState from '../initialState'
import { FETCH_PRODUCTS_LOADING,
         FETCH_PRODUCTS_SUCCESS,
         FETCH_PRODUCTS_ERROR,
         SET_PRODUCT_TAG_FILTER,
         GET_PRODUCT_DETAILS} from '../actionTypes'

export default (state = initialState.product, action) => {

  switch(action.type) {

    case FETCH_PRODUCTS_LOADING: {
      return {
        ...state,
        loading: true,
        error: ''
      }
    }

    case FETCH_PRODUCTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        products: action.products
      }
    }

    case FETCH_PRODUCTS_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error
      }
    }

    case SET_PRODUCT_TAG_FILTER: {
      return {
        ...state,
        tagFilter: action.tag
      }
    }

    //TODO use selector
    case GET_PRODUCT_DETAILS: {
      let {id} = action
      let productDetail = state.products.find((product)=> product.id === id)
      return {
        ...state,
        productDetail: productDetail
      }
    }

    default:
      return {...state}
  }
}

/***** SELECTORS *****/
export function getProducts(state) {
  if (state.tagFilter === '') {
    return state.products
  } else {
    return state.products.filter(product => product.tags.includes(state.tagFilter))
  }
}

export function getProduct(id, state) {
  return state.products.find((product)=> product.id === id)
}

